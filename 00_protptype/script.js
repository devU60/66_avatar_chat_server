'use strict';


//entry point :
function main(){
    THREE.JeelizHelper.init({
        canvasThreeId: 'webojiCanvas',
        canvasId: 'jeefacetransferCanvas',

        assetsParentPath: './assets/3D/',
        NNCpath: './dist/',

        
        //FOX :
        meshURL: 'meshes/fox11_v0.json',
        matParameters: {
          diffuseMapURL: 'textures/Fox_albedo.png',
          specularMapURL: 'textures/Fox_specular.png',
          flexMapURL: 'textures/Fox_flex.png'
        }, 

        /*
        meshURL: 'meshes/cartman.json',
        matParameters: {
          diffuseMapURL: 'diffuse_cartman.jpg',
          //specularMapURL: 'textures/Fox_specular.png',
          //flexMapURL: 'textures/Fox_flex.png'
        },
        */ 

        
        /*
        meshURL: 'meshes/moca3.json',
        matParameters: {
          diffuseMapURL: 'textures/skin_eye.png',
          specularMapURL: 'textures/skin_eye.png',
          flexMapURL: 'textures/skin_eye.png'
        },
        */


      position: [0,-80,0],
      scale: 1.2
    });
} //end main()

main();



var lStream = null;
let peer = null;
let existingCall = null;
var canvas = document.getElementById('webojiCanvas');
var localStream;



function test(){


    navigator.mediaDevices.getUserMedia({video: false, audio: true})
        .then(function (stream) {
            // Success
            //$('#my-video').get(0).srcObject = stream;
            lStream = stream;
            const cStream = canvas.captureStream(12);
            const [vTrack] = cStream.getVideoTracks();
            const [aTrack] = lStream.getAudioTracks();
            localStream = new MediaStream([vTrack, aTrack]);

        }).catch(function (error) {
            // Error
            console.error('mediaDevice.getUserMedia() error:', error);
            return;
        });


    peer = new Peer({
        key: '22cc6716-4e32-4c63-a809-95afed311ee4',
        debug: 3
    });


    peer.on('open', function(){
        $('#my-id').text(peer.id);
    });


    peer.on('error', function(err){
        alert(err.message);
    });


    peer.on('close', function(){
    });

    peer.on('disconnected', function(){
    });

    $('#make-call').submit(function(e){
        e.preventDefault();
        const call = peer.call($('#callto-id').val(), localStream);
        setupCallEventHandlers(call);
    });


    $('#end-call').click(function(){
        existingCall.close();
    });


    peer.on('call', function(call){
        call.answer(localStream);
        setupCallEventHandlers(call);
    });

}




function setupCallEventHandlers(call){

    if (existingCall) {
        existingCall.close();
    };

    existingCall = call;

    // 省略
    call.on('stream', function(stream){
        addVideo(call,stream);
        setupEndCallUI();
        $('#their-id').text(call.remoteId);
    });
    // 省略

    call.on('close', function(){
        removeVideo(call.remoteId);
        setupMakeCallUI();
    });
}


function addVideo(call,stream){
    $('#their-video').get(0).srcObject = stream;
}


function removeVideo(peerId){
    $('#their-video').get(0).srcObject = undefined;
}

function setupMakeCallUI(){
    $('#make-call').show();
    $('#end-call').hide();
}

function setupEndCallUI() {
    $('#make-call').hide();
    $('#end-call').show();
}


