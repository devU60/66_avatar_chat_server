const Peer = window.Peer;

"use strict";

//entry point :
function main_weboji(){
    THREE.JeelizHelper.init({
        canvasThreeId: 'webojiCanvas',
        canvasId: 'jeefacetransferCanvas',

        assetsParentPath: '../../../assets/3D/',
        NNCpath: '../../../dist/',

        //FOX :
        meshURL: 'meshes/fox11_v0.json',
        matParameters: {
          diffuseMapURL: 'textures/Fox_albedo.png',
          specularMapURL: 'textures/Fox_specular.png',
          flexMapURL: 'textures/Fox_flex.png'
        }, //*/

      //KOALA :
      /*meshURL: 'meshes/koala.json', //mesh loaded by default - custom face
      matParameters: {
            diffuseMapURL: 'textures/diffuse_koala.jpg'
          }, //*/

      //HUMAN CREEPY FACE :
      /*meshURL: 'meshes/faceCustom11_v0.json',
        matParameters: {
            diffuseMapURL: 'textures/skin.jpg'
        },  //*/

      position: [0,-80,0],
      scale: 1.2
    });
} //end main()


//(async function main() {
$(document).ready(function () {
  //main_weboji();
  const localVideo = document.getElementById('js-local-stream');
  const localId = document.getElementById('js-local-id');
  const callTrigger = document.getElementById('js-call-trigger');
  const closeTrigger = document.getElementById('js-close-trigger');
  const remoteVideo = document.getElementById('js-remote-stream');
  const remoteId = document.getElementById('js-remote-id');
  // capture canvas 
  const canvas = document.getElementById('webojiCanvas');


  //const lStream = await navigator.mediaDevices
  const lStream = navigator.mediaDevices
    .getUserMedia({
      audio: true,
      video: false,
    })
    .catch(console.error);

  
  //const canvasStream = await canvas.captureStream(10);
  const canvasStream = canvas.captureStream(10);
  const [vTrack] = canvasStream.getVideoTracks();
  const [aTrack] = lStream.getAudioTracks();
  const localStream = new MediaStream([vTrack, aTrack]);


  // Render local stream
  localVideo.muted = true;
  localVideo.srcObject = localStream;
  localVideo.playsInline = true;
  //await localVideo.play().catch(console.error);
  localVideo.play().catch(console.error);
  

  const peer = new Peer({
    key: window.__SKYWAY_KEY__,
    debug: 3,
  });

  // Register caller handler
  callTrigger.addEventListener('click', () => {
    // Note that you need to ensure the peer has connected to signaling server
    // before using methods of peer instance.
    if (!peer.open) {
      return;
    }

    const mediaConnection = peer.call(remoteId.value, localStream);

    mediaConnection.on('stream', async stream => {
      // Render remote stream for caller
      remoteVideo.srcObject = stream;
      remoteVideo.playsInline = true;
      //await remoteVideo.play().catch(console.error);
      remoteVideo.play().catch(console.error);
    });

    mediaConnection.once('close', () => {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
      remoteVideo.srcObject = null;
    });

    closeTrigger.addEventListener('click', () => mediaConnection.close(true));
  });

  peer.once('open', id => (localId.textContent = id));

  // Register callee handler
  peer.on('call', mediaConnection => {
    mediaConnection.answer(localStream);

    mediaConnection.on('stream', async stream => {
      // Render remote stream for callee
      remoteVideo.srcObject = stream;
      remoteVideo.playsInline = true;
      //await remoteVideo.play().catch(console.error);
      remoteVideo.play().catch(console.error);
    });

    mediaConnection.once('close', () => {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
      remoteVideo.srcObject = null;
    });

    closeTrigger.addEventListener('click', () => mediaConnection.close(true));
  });

  peer.on('error', console.error);
});
//})();
